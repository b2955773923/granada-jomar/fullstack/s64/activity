let collection = [];

// Write the queue functions below.

function print() {
  console.log(collection);
  return collection
}

function enqueue(itemA) {
	collection[collection.length] = itemA;
	return collection;
  }
  
  function dequeue() {
	const removedElement = collection[0];
  
	for (let i = 0; i < collection.length - 1; i++) {
	  collection[i] = collection[i + 1];
	}
  
	collection.length--;
  
	return collection;
  }
function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};